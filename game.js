//gameAlphabet//
function randomAlphaBtn() {
    var text = [];
    var textplus = ['kh', 'nh', 'ch', 'ph', 'tr', 'ng', 'ngh', 'gh', 'ă', 'â', 'đ', 'ê', 'ô', 'ơ', 'ư'];
    for (let i = 97; i < 123; i++) {
        const alpha = String.fromCharCode(i);
        text.push(alpha)
    }
    var totalAlphabet = text.concat(textplus);
    var random = Math.floor(Math.random() * totalAlphabet.length);
    //  console.log(random)
    const renderAlpha = totalAlphabet[random];
    document.getElementById('text').innerHTML = renderAlpha;
}

//math//
//reload page everything will be 0
document.getElementById('fisrtNum').innerHTML = 0;
document.getElementById('symbol').innerHTML = '+/-';
document.getElementById('secNum').innerHTML = 0;
document.getElementById('showResult').innerHTML = 0;
// getElement the 'square place' where show the final result
const showMathResult = document.getElementById('showResult');

function symbol() {
    const randomNum = Math.floor(Math.random() * 2);
    var randomSymbol = '';
    if (randomNum === 0) {
        randomSymbol += '+';
    } else {
        randomSymbol += "-";
    }
    return randomSymbol;
}

var mathArray = [];

function resetNumberBtn() {
    var showSymbol = symbol();
    //console.log(showSymbol)
    document.getElementById('symbol').innerHTML = showSymbol;
    mathArray = [];
    for (let i = 0; i < 2; i++) {
        mathArray.push(Math.floor(Math.random() * 49));
    }
    var biggerNum;
    var smallerNum;
    if (showSymbol === "+") {
        document.getElementById('fisrtNum').innerHTML = mathArray[0];
        document.getElementById('secNum').innerHTML = mathArray[1];
        document.getElementById('showResult').innerHTML = mathArray[0] + mathArray[1];
    } else if (showSymbol === "-" && mathArray[0] < mathArray[1]) {
        biggerNum = mathArray[1];
        smallerNum = mathArray[0];
        document.getElementById('fisrtNum').innerHTML = biggerNum;
        document.getElementById('secNum').innerHTML = smallerNum;
        document.getElementById('showResult').innerHTML = biggerNum - smallerNum;
    } else {
        document.getElementById('fisrtNum').innerHTML = mathArray[0];
        document.getElementById('secNum').innerHTML = mathArray[1];
        document.getElementById('showResult').innerHTML = mathArray[0] - mathArray[1];
    }
    // make the input result && content === '' when reset
    document.getElementById('input').value = '';
    document.getElementById('math-contentResult').innerHTML = "";
    //hidden the 'square place' until you type true result
    showMathResult.style.display = 'none';
}

function resultBtn() {
    var getInput = document.getElementById('input').value;
    var trueResult = document.getElementById('showResult').innerHTML;
    var placeAddContent = document.getElementById('math-contentResult');
    if (getInput === trueResult) {
        placeAddContent.innerHTML = "Hooray! Congratulations!";
        //after click 'result button',ur result is true => show the 'square place' where show the final result
        showMathResult.style.display = 'inline-block';
    } else {
        placeAddContent.innerHTML = " Oh no! Let's try again!";
        //still hidden the 'square place' show the final result if ur result is not true
        showMathResult.style.display = 'none';
    }
}

//Fill the blank with >,<,=//
var signFillarray = [];

function signFill_ResetBtn() {
    // after reset, array will be 0
    signFillarray = [];
    for (let i = 0; i < 2; i++) {
        signFillarray.push(Math.floor(Math.random() * 99)); //push item into global var
    }
    document.getElementById('signFill-fisrtNum').innerHTML = signFillarray[0];
    document.getElementById('signFill-secNum').innerHTML = signFillarray[1];
    document.getElementById('signFill-input').value = "?";
    document.getElementById('signFill-contentResult').innerHTML = "";
}

function signFill_PassValue(valSymbol) {
    document.getElementById('signFill-input').value = valSymbol.value
}

// pass 'global variable' named array (arr) into func
function signFill_symbol(arr) {
    var first = arr[0];
    var sec = arr[1];
    var symbol = '';
    if (first === sec) {
        symbol += '=';
    } else if (first < sec) {
        symbol += '<';
    } else if (first > sec) {
        symbol += '>';
    }
    return symbol;
}

function signFill_ResultBtn() {
    var sym = signFill_symbol(signFillarray); //get symbol =  func pass array to compare
    var getInputValue = document.getElementById('signFill-input').value;
    if (sym === getInputValue) {
        document.getElementById('signFill-contentResult').innerHTML = "Hooray! Congratulations!";
    } else {
        document.getElementById('signFill-contentResult').innerHTML = "Oh no! Let's try again!";
    }
}

//choose answer//
var chooseAnswerArray = [];
var randomQuestion;
const SMALLEST_LARGET_VALUES = {
    smallest: 'the smallest',
    largest: 'the largest'
};

function chooseAnswer_Number() {
    var question = Object.values(SMALLEST_LARGET_VALUES);

    randomQuestion = question[Math.floor(Math.random() * question.length)];
    var finalQuestion = `Which one is <span class="redText ">${randomQuestion}</span> number?`;
    document.getElementById('chooseAnswer-question').innerHTML = finalQuestion;
    chooseAnswerArray = [];
    while (chooseAnswerArray.length < 5) {var randomNumber = Math.floor(Math.random() * 99)
        if (!chooseAnswerArray.includes(randomNumber)) {
            chooseAnswerArray.push(randomNumber)
        }
    }
    document.getElementById('choseAnswer-fisrtNum').innerHTML = chooseAnswerArray[0];
    document.getElementById('choseAnswer-secNum').innerHTML = chooseAnswerArray[1];
    document.getElementById('choseAnswer-thirdNum').innerHTML = chooseAnswerArray[2];
    document.getElementById('choseAnswer-fourthNum').innerHTML = chooseAnswerArray[3];
    document.getElementById('choseAnswer-fifthNum').innerHTML = chooseAnswerArray[4];
    document.getElementById('chooseAnswer-contentResult').innerHTML = '';
    // remove class 'changeColor' when reset
    // choose class got class 'square'+'changeColor'
    var getClass = document.querySelectorAll('.square.changeColor')
    for (let j = 0; j < getClass.length; j++) {
        getClass[j].classList.remove('changeColor')
    }
}

var UserChoose;

// get value when click any button
function getUserChoosen(event) {
    // value in span tag, so through div ,get 1 child is span tag
    var getChildFromDiv = event.childNodes
    //console.log(getChildFromDiv[0])
    UserChoose = getChildFromDiv[0].innerHTML;
    //console.log(UserChoose)
    // find out elemnt have className changecColor, and remove that className
    var getAllElementSameClassName = document.querySelectorAll('.changeColor');
    console.log(getAllElementSameClassName)
    for (i = 0; i < getAllElementSameClassName.length; i++) {
        getAllElementSameClassName[i].classList.remove("changeColor")
    }

    event.classList.toggle('changeColor');
}

function chooseAnswer_Result() {
    var Choosen = parseInt(UserChoose)
    console.log('userchoose', Choosen)
    var biggest = Math.max(...chooseAnswerArray);
    var smallest = Math.min(...chooseAnswerArray);
    console.log(biggest)
    console.log(smallest)
    console.log(randomQuestion)
    if (randomQuestion === SMALLEST_LARGET_VALUES.smallest && Choosen === smallest) {
        document.getElementById('chooseAnswer-contentResult').innerHTML = "Hooray! Congratulations!"
    } else if (randomQuestion === SMALLEST_LARGET_VALUES.largest && Choosen === biggest) {
        document.getElementById('chooseAnswer-contentResult').innerHTML = "Hooray! Congratulations!"
    } else {
        document.getElementById('chooseAnswer-contentResult').innerHTML = "Oh no! Let's try again!"
    }
}

//Arrange Number//
//create 2 varArray , one for sort, one for reverse
var arrangeNumberArray = [];
var arrangeNumberArray2;
var question = ['Smallest To Largest', 'Largest To Smallest']
var arrangeNumberQuestion;

function arrangeNumber_Reset() {
    arrangeNumberQuestion = question[Math.floor(Math.random() * question.length)];
    var finalQuestion = `Arrange The Numbers From <span class="redText ">${arrangeNumberQuestion}</span>`;
    document.getElementById('arrangeNumber-question').innerHTML = finalQuestion;
    // restet 2 array when onclick
    arrangeNumberArray = []
    arrangeNumberArray2 = []
    while (arrangeNumberArray.length < 5) {
        var randomNumber = Math.floor(Math.random() * 99);
        if (!arrangeNumberArray.includes[randomNumber]) {
            arrangeNumberArray.push(randomNumber)
        }
        arrangeNumberArray2 = [...arrangeNumberArray] // clone value arr for arr2
    }
    //console.table({arrangeNumberArray, arrangeNumberArray2})
    var getClassquestion = document.querySelectorAll('.arrangeNumber-question')
    for (let i = 0; i < getClassquestion.length; i++) {
        getClassquestion[i].innerHTML = arrangeNumberArray[i];
    }
    document.getElementById('arrangeNumber-contentResult').innerHTML = '';
    //reset input equal 0 when reset
    var getresetanswer = document.querySelectorAll('.userAnswer')
    getresetanswer.forEach((itemAnswer) => itemAnswer.value = '') //print out
}

var arrayUserInput = []

function arrangeNumber_Result() {
    //reset old value first , and push new value
    arrayUserInput = []
    var userInput = document.querySelectorAll('.userAnswer')
    for (let i = 0; i < userInput.length; i++) {
        arrayUserInput.push(parseInt(userInput[i].value))
    }
    var smallToLarge = arrangeNumberArray.sort((a, b) => a - b) // array small to large
    var largeToSmall = arrangeNumberArray2.sort((a, b) => b - a) // array2 large to small    
    var compareSmallToLarge = smallToLarge.every((item, index) => item === arrayUserInput[index])
    var compareLargeToSmall = largeToSmall.every((item, index) => item === arrayUserInput[index])
    console.log({arrangeNumberQuestion, compareSmallToLarge, compareLargeToSmall})
    if (arrangeNumberQuestion === question[0] && compareSmallToLarge === true) {
        document.getElementById('arrangeNumber-contentResult').innerHTML = "Hooray! Congratulations!"
    } else if (arrangeNumberQuestion === question[1] && compareLargeToSmall === true) {
        document.getElementById('arrangeNumber-contentResult').innerHTML = "Hooray! Congratulations!"
    } else {
        document.getElementById('arrangeNumber-contentResult').innerHTML = "Oh no! Let's try again!"
    }
}

//Find previous number and next number//
var prevAndnextRandomNumber;

function prevAndnext_reset() {
    prevAndnextRandomNumber = Math.floor(Math.random() * 98) + 1;
    document.getElementById('prevAndnext-firstinput').value = '';
    document.getElementById('prevAndnext-randomNumber').innerHTML = prevAndnextRandomNumber;
    document.getElementById('prevAndnext-secinput').value = '';
    document.getElementById('prevAndnext-contentResult').innerHTML = '';
}

function prevAndnext_result() {
    var prevnum = prevAndnextRandomNumber - 1;
    var nextnum = prevAndnextRandomNumber + 1;
    var prevInput = parseInt(document.getElementById('prevAndnext-firstinput').value);
    var nextInput = parseInt(document.getElementById('prevAndnext-secinput').value);
    if (prevnum === prevInput) {
        document.getElementById('prevAndnext-contentResult').innerHTML = "Hooray! Congratulations!";
    }
    if (nextnum === nextInput) {
        document.getElementById('prevAndnext-contentResult').innerHTML = "Hooray! Congratulations!";
    } else {
        document.getElementById('prevAndnext-contentResult').innerHTML = "Oh no! Let's try again!";
    }

}


//Comparing calculations//
var compareCal_randomleftNum;
var compareCal_randomSymbol;
var compareCal_arrayRightNum;
var compareCal_total;
const symbolcompareCal = {
    plus: "+",
    substract: "-"
};

function compareCal_reset() {
    const arraySymbol = Object.values(symbolcompareCal)
    compareCal_randomSymbol = arraySymbol[Math.floor(Math.random() * 2)]
    compareCal_randomleftNum = Math.floor(Math.random() * 98)
    compareCal_arrayRightNum = [];
    while (compareCal_arrayRightNum.length < 2) {
        var randomRightNum = Math.floor(Math.random() * 60);
        if (!compareCal_arrayRightNum[randomRightNum]) {
            compareCal_arrayRightNum.push(randomRightNum)
        }
    }
    document.getElementById('compareCal-leftNum').innerHTML = compareCal_randomleftNum;
    document.getElementById('compareCal-symbol').innerHTML = compareCal_randomSymbol;
    document.getElementById('compareCal-inputSymbol').value = "";
    document.getElementById('compareCal-contentResult').innerHTML = "";
    var largeNum;
    var smallNum;
    if (compareCal_randomSymbol === symbolcompareCal.plus) {
        document.getElementById('compareCal-rightFirstNum').innerHTML = compareCal_arrayRightNum[0];
        document.getElementById('compareCal-rightSectNum').innerHTML = compareCal_arrayRightNum[1];
        compareCal_total = compareCal_arrayRightNum[0] + compareCal_arrayRightNum[1]
    } else if (compareCal_randomSymbol === symbolcompareCal.substract && compareCal_arrayRightNum[0] < compareCal_arrayRightNum[1]) {
        largeNum = compareCal_arrayRightNum[1]
        smallNum = compareCal_arrayRightNum[0]
        document.getElementById('compareCal-rightFirstNum').innerHTML = largeNum;
        document.getElementById('compareCal-rightSectNum').innerHTML = smallNum;
        compareCal_total = largeNum - smallNum
    } else {
        document.getElementById('compareCal-rightFirstNum').innerHTML = compareCal_arrayRightNum[0];
        document.getElementById('compareCal-rightSectNum').innerHTML = compareCal_arrayRightNum[1];
        compareCal_total = compareCal_arrayRightNum[0] - compareCal_arrayRightNum[1]
    }
}

function compareCal_PassValue(getSymbol) {
    document.getElementById('compareCal-inputSymbol').value = getSymbol.value
}

function compareCal_result() {
    var userInput = document.getElementById('compareCal-inputSymbol').value;
    if (compareCal_randomleftNum < compareCal_total && userInput === "<") {
        document.getElementById('compareCal-contentResult').innerHTML = "Hooray! Congratulations!";
    } else if (compareCal_randomleftNum > compareCal_total && userInput === ">") {
        document.getElementById('compareCal-contentResult').innerHTML = "Hooray! Congratulations!";
    } else if (compareCal_randomleftNum === compareCal_total && userInput === "=") {
        document.getElementById('compareCal-contentResult').innerHTML = "Hooray! Congratulations!";
    } else {
        document.getElementById('compareCal-contentResult').innerHTML = "Oh no! Let's try again!";
    }
}